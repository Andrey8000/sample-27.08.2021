package com.example.sample27_08_2021_2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.example.sample27_08_2021_2.databinding.ActivityMainBinding
import com.example.sample27_08_2021_2.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val viewModel: MainViewModel by viewModels()

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel.title.observeWhenStarted(this) {
            Log.v("test_", "MainActivity observeWhenStarted")
            binding.title.text = it
        }
    }
}

private fun <T : Any> Flow<T>.observeWhenStarted(activity: AppCompatActivity, action: (T) -> Unit) {
    activity.lifecycleScope.launch {

        //    Experimental now:
        //    viewModel.title
        //        .flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
        //        .collect {
        //
        //        }

        // repeatOnLifecycle launches the block in a new coroutine every time the
        // lifecycle is in the STARTED state (or above) and cancels it when it's STOPPED.
        activity.repeatOnLifecycle(Lifecycle.State.STARTED) {
            // Trigger the flow and start listening for values.
            // Note that this happens when lifecycle is STARTED and stops
            // collecting when the lifecycle is STOPPED
            this@observeWhenStarted.collect {
                action(it)
            }
        }
    }
}