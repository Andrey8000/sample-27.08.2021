package com.example.sample27_08_2021_2.data

import android.util.Log
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TitleRepository @Inject constructor(
    private val cnt: Counter,
) {

    //private var titleLoadCnt = 0
    private var subTitleLoadCnt = 0

    suspend fun loadTitle(): Flow<String> = flow {
        while(true) {
            cnt.cnt++
            Log.d("test_", "loadTitle emit")
            emit("Title: ${cnt.cnt}")

            // delay(1000)
            // Emulate blocking operation
            Thread.sleep(1000)
        }
    }

    suspend fun loadSubTitle(): Flow<String> = flow {
        while(true) {
            subTitleLoadCnt++
            Log.d("test_", "loadSubTitle emit")
            emit("Subtitle: $subTitleLoadCnt")

            // delay(500)
            // Emulate blocking operation
            Thread.sleep(2000)
        }
    }
}