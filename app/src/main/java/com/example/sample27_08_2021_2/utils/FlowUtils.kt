package com.example.sample27_08_2021_2.utils

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn

fun <T : Any> Flow<T>.onIo() = flowOn(Dispatchers.IO)