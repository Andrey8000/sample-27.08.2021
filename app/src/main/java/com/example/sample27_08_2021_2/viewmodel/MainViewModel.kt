package com.example.sample27_08_2021_2.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.sample27_08_2021_2.data.TitleRepository
import com.example.sample27_08_2021_2.utils.onIo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val titleRepository: TitleRepository,
) : ViewModel() {

    init {
        Log.i("test_", "MainViewModel init. $titleRepository")

        viewModelScope.launch {
            //withContext(Dispatchers.IO) {
                titleRepository.loadTitle()//.onIo()
                    .combine(titleRepository.loadSubTitle()/*.onIo()*/) { s1, s2 ->
                        Log.i("test_", "MainViewModel combine")
                        "$s1\n$s2"
                    }
                    .onIo()
                    .collectLatest {
                        Log.i("test_", "MainViewModel collect")
                        _title.value = it
                    }
            //}
        }
    }

    private val _title = MutableStateFlow("")
    val title: Flow<String> = _title
}